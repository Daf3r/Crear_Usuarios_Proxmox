#!/bin/bash

# Verificar si se proporciona un argumento (nombre de usuario)
if [[ $# -ne 1 ]]; then
    echo "Este script es para los contenedores recien instalados de proxmox"
    echo "Uso: $0 <nombre_de_usuario>"
    exit 1
fi

# Actualizar el sistema
apt update && apt upgrade -y

# Crear el usuario y establecer contraseña
USER="$1"
useradd -m "$USER"
passwd "$USER"

# Cambiar el shell del usuario a /bin/bash
chsh -s /bin/bash "$USER"

echo "Usuario $USER creado correctamente con shell /bin/bash."

